# Gecos-updateSite

#### Welcome to the update site for Gecos. 

This site contains the references to all the dependencies needed for developement.

Use this url:
```
https://gecos.gitlabpages.inria.fr/gecos-updatesite
```

<h5> For Eclipse users : </h5>
<img src="images/image.png" alt="Where to find install window" width="25%"/>\
<h5> Then : </h5>
<img src="images/install.png" alt="Install window" width="50%"/>

<h5> This link contains the dependencies : </h5> 
 <ul>
    <li>gecos-tools-tomsdk</li>
    <li>gecos-tools-tommapping</li>
    <li>gecos-tools-emf</li>
    <li>gecos-tools-jnimapper</li>
    <li>gecos-tools-graph</li>
    <li>gecos-tools-isl</li>
    <li>gecos-core</li>
</ul>
